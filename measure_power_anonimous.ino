/*
Thanks to https://www.instructables.com/Arduino-Energy-Meter-V20/
 */
#include <SPI.h>
#include <Ethernet.h>

// Enter a MAC address for your controller below.
// Newer Ethernet shields have a MAC address printed on a sticker on the shield
byte mac[] = { 0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED };

// if you don't want to use DNS (and reduce your sketch size)
// use the numeric IP instead of the name for the server:
IPAddress server(52, 204, 34, 129);  // numeric IP for thingspeak (no DNS)
//char server[] = "api.thingspeak.com";    // name address for thingspeak (using DNS)

// Set the static IP address to use if the DHCP fails to assign
IPAddress ip(..., ..., ..., ...);
IPAddress myDns(..., ..., ..., ...);

// Initialize the Ethernet client library
// with the IP address and port of the server
// that you want to connect to (port 80 is default for HTTP):
EthernetClient client;


const float Vcc_house = 220.0;
const float power_factor = 0.85;

const int Sensor_Pin = A0;
const int potentiometer_pin = A1;

const float sensitivity = 100.0; // 1A/100mV


void setup() {
  // You can use Ethernet.init(pin) to configure the CS pin
  Ethernet.init(10);  // Most Arduino shields
  //Ethernet.init(5);   // MKR ETH Shield
  //Ethernet.init(0);   // Teensy 2.0
  //Ethernet.init(20);  // Teensy++ 2.0
  //Ethernet.init(15);  // ESP8266 with Adafruit FeatherWing Ethernet
  //Ethernet.init(33);  // ESP32 with Adafruit FeatherWing Ethernet

  // Open serial communications and wait for port to open:
  Serial.begin(9600);
  while (!Serial) {
    ; // wait for serial port to connect. Needed for native USB port only
  }

  // start the Ethernet connection:
  Serial.println("Initialize Ethernet with DHCP:");
  if (Ethernet.begin(mac) == 0) {
    Serial.println("Failed to configure Ethernet using DHCP");
    // Check for Ethernet hardware present
    if (Ethernet.hardwareStatus() == EthernetNoHardware) {
      Serial.println("Ethernet shield was not found.  Sorry, can't run without hardware. :(");
      while (true) {
        delay(1); // do nothing, no point running without Ethernet hardware
      }
    }
    if (Ethernet.linkStatus() == LinkOFF) {
      Serial.println("Ethernet cable is not connected.");
    }
    // try to configure using IP address instead of DHCP:
    Ethernet.begin(mac, ip, myDns);
  } else {
    Serial.print("  DHCP assigned IP ");
    Serial.println(Ethernet.localIP());
  }


  // if you get a connection, report back via serial:
}

void loop() {
  delay(1000)

  float calibration = analogRead(A1) / 10.0;
  float interpolation_quote = 0;
  float interpolation_multiplier = 5;

  float sumValue = 0;
  int maxVal = 0;
  int minVal = 1024;
  float readValue;
  uint32_t start_time = millis();
  while((millis()-start_time) < 950){ //read every 0.95 Sec    
    readValue = analogRead(A0);
    minVal = readValue < minVal ? readValue : minVal;
    maxVal = readValue > maxVal ? readValue : maxVal;
  }
  float Vpp = (maxVal - minVal) / 1024.0;
  float Vrms = (Vpp/2.0) * 0.707; 
  Vrms = Vrms; // - (calibration);     // calibtrate to zero with slider
  float Irms = (Vrms * 1000) / sensitivity;
  if((Irms > -0.015) && (Irms < 0.008)){  // remove low end chatter
    Irms = 0.0;
  }
  float active_power = (Vcc_house * Irms) * power_factor; 
  Serial.print("\n");
  Serial.print((maxVal - minVal) * interpolation_multiplier + interpolation_quote);
  Serial.print(",");
  Serial.print(Irms);
  Serial.print(",");
  Serial.print(active_power + 25 - calibration);
  Serial.print(",");
  Serial.print(calibration);


  // give the Ethernet shield a second to initialize:  
  Serial.print("connecting to ");
  Serial.print(server);
  Serial.println("...");
  if (client.connect(server, 80)) {
    Serial.print("connected to ");
    Serial.println(client.remoteIP());
    // Make a HTTP request:
    client.println("GET /update?api_key=...&field1="+String((maxVal - minVal))+"&field2="+String(active_power + 25 - calibration));
    client.println("Host: api.thingspeak.com");
    client.println("Connection: close");
    client.println();
  } else {
    // if you didn't get a connection to the server:
    Serial.println("connection failed");
  }
  client.stop();

  delay(10000);
}


